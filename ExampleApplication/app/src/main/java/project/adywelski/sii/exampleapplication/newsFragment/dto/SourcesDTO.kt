package project.adywelski.sii.exampleapplication.newsFragment

/**
 * Created by ariel_dywelski on 07/11/2017.
 */
data class SourcesDTO(val id: String,
                      val name: String)