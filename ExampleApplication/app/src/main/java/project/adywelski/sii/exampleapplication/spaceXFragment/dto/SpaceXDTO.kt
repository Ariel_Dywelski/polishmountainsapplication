package project.adywelski.sii.exampleapplication.spaceXFragment.dto

/**
 * Created by ariel_dywelski on 14/11/2017.
 */
data class SpaceXDTO (val flight_number: Int,
                      val launch_data_utc: String,
                      val details: String,
                      val rocket: String,
                      val links: SpaceXLinksDTO)