package project.adywelski.sii.exampleapplication.swipeLayout

import android.support.v7.widget.RecyclerView

/**
 * Created by ariel_dywelski on 29/11/2017.
 */
interface RecyclerItemTouchHelperListener {
    fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int, position: Int)
}