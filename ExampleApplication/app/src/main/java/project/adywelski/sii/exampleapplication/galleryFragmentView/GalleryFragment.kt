package project.adywelski.sii.exampleapplication.galleryFragmentView

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import project.adywelski.sii.exampleapplication.MainApplication
import project.adywelski.sii.exampleapplication.R
import project.adywelski.sii.exampleapplication.galleryFragmentView.dto.PictureDTO
import project.adywelski.sii.exampleapplication.galleryFragmentView.dto.UnsplashDTO
import project.adywelski.sii.exampleapplication.galleryFragmentView.service.UnsplashWebService
import project.adywelski.sii.exampleapplication.galleryFragmentView.view.GalleryRecyclerViewAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class GalleryFragment : Fragment() {

    private val appKey: String = "https://api.unsplash.com/photos?client_id=64afac134e59142ed6e01dad383f6f6335b4e708b88f56ef446c47349e37c96b&per_page=30&order_by=popular"

    @Inject lateinit var pixabayWebService: UnsplashWebService
    lateinit var recyclerView : RecyclerView

    override fun onAttach(context: Context) {
        super.onAttach(context)

        ((context.applicationContext as MainApplication)).getAppComponent().inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val returnView: View = inflater!!.inflate(R.layout.gallery_main, container, false)

        recyclerView = returnView.findViewById(R.id.pictureRecyclerView)

        getPicture()

        return returnView
    }

    private fun getPicture(){
        val pictureUrl : ArrayList<PictureDTO> = arrayListOf()
        pixabayWebService.getUnsplashPhoto(appKey).enqueue(object : Callback<List<UnsplashDTO>>{
            override fun onFailure(call: Call<List<UnsplashDTO>>?, t: Throwable?) {
                Toast.makeText(context, "Failure", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<List<UnsplashDTO>>?, response: Response<List<UnsplashDTO>>?) {
                if (response != null) {
                    if (response.isSuccessful){
                        val body : List<UnsplashDTO> = response.body()!!

                        (0 until body.size).mapTo(pictureUrl) { body[it].urls }

                        if(body.isEmpty()) {
                            Toast.makeText(context, "Noooo", Toast.LENGTH_SHORT).show()
                        }

                        val pictureRecyclerViewAdapter = GalleryRecyclerViewAdapter(context, pictureUrl)

                        recyclerView.adapter = pictureRecyclerViewAdapter
                        recyclerView.layoutManager = GridLayoutManager(context, 2)

                        Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show()
                    }
                }
            }

        })
    }
}

