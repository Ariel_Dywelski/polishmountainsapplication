package project.adywelski.sii.exampleapplication.module

import dagger.Module
import dagger.Provides
import project.adywelski.sii.exampleapplication.MainApplication
import javax.inject.Singleton

/**
 * Created by ariel_dywelski on 31/10/2017.
 */
@Module
class AndroidModule(private val application: MainApplication) {

    @Provides
    @Singleton
    fun provideApplication() = application
}