package project.adywelski.sii.exampleapplication.galleryFragmentView.view

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import project.adywelski.sii.exampleapplication.R
import project.adywelski.sii.exampleapplication.galleryFragmentView.dto.PictureDTO

/**
 * Created by ariel_dywelski on 03/11/2017.
 */
class GalleryRecyclerViewAdapter(val context: Context, private val pictureList: ArrayList<PictureDTO>) : RecyclerView.Adapter<GalleryViewHolder> () {

    private val layoutInflater : LayoutInflater = LayoutInflater.from(context)

    override fun getItemCount(): Int {
        return pictureList.size
    }

    override fun onBindViewHolder(holder: GalleryViewHolder?, position: Int) {
        val pictureDTO : PictureDTO = pictureList[position]

        val pictureView : ImageView = holder!!.picture

        Glide.with(context)
                .load(pictureDTO.small)
                .into(pictureView)

        holder.pictureClick(context, pictureDTO)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): GalleryViewHolder {
        val view : View = layoutInflater.inflate(R.layout.gallery_main_view_item, parent, false)
        return GalleryViewHolder(view)
    }
}