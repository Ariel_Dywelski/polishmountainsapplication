package project.adywelski.sii.exampleapplication.newsFragment.view

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import project.adywelski.sii.exampleapplication.R
import project.adywelski.sii.exampleapplication.newsFragment.ArticlesDTO

/**
 * Created by ariel_dywelski on 27/10/2017.
 */
class NewsRecyclerViewAdapter(val context: Context, private val articleList: List<ArticlesDTO>) : RecyclerView.Adapter<NewsViewHolder>()  {

    private val layoutInflater : LayoutInflater = LayoutInflater.from(context)

    override fun onBindViewHolder(holder: NewsViewHolder?, position: Int) {
        if (holder != null) {
            val articleDTO: ArticlesDTO = articleList[position]

            holder.bind(articleDTO)
        }
    }

    override fun getItemCount(): Int {
        return articleList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): NewsViewHolder {
        val view : View = layoutInflater.inflate(R.layout.fragment_news_view_item, parent, false)
        return NewsViewHolder(view, context)
    }

}