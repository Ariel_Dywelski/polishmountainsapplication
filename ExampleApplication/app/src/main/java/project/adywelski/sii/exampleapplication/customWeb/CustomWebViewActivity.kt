package project.adywelski.sii.exampleapplication.customWeb

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.design.widget.CollapsingToolbarLayout
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.view.MotionEvent
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageView
import android.widget.ProgressBar
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import kotlinx.android.synthetic.main.activity_custom_web_view.*
import project.adywelski.sii.exampleapplication.MainApplication
import project.adywelski.sii.exampleapplication.R

class CustomWebViewActivity : AppCompatActivity() {

    private lateinit var customWeb: WebView
    private lateinit var progressBar: ProgressBar
    private lateinit var imageHeader: ImageView
    private lateinit var utils: Utils

    private var mDownX: Float = 0f
    private var postUrl = "https://api.androidhive.info/webview/index.html"

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_custom_web_view)
        setSupportActionBar(webToolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        ((applicationContext as MainApplication)).getAppComponent().inject(this)

        customWeb = findViewById(R.id.customWebView)
        progressBar = findViewById(R.id.webProgressBar)
        imageHeader = findViewById(R.id.backdrop)
        utils = Utils()

        if (!TextUtils.isEmpty(intent.getStringExtra("postUrl"))) {
            postUrl = intent.getStringExtra("postUrl")
        }

        initWebView()
        initCollapsingToolbar()
        renderPosition()

        customWeb.settings.javaScriptEnabled = true
        customWeb.settings.setSupportZoom(true)
        customWeb.settings.builtInZoomControls = true
        customWeb.settings.displayZoomControls = true

        customWeb.loadUrl("file:///android_asset/sample.html")
        customWeb.isHorizontalScrollBarEnabled = false
    }

    @SuppressLint("ClickableViewAccessibility", "SetJavaScriptEnabled")
    private fun initWebView() {
        customWeb.webChromeClient = CustomWebViewActivity.MyWebChromeClient(applicationContext)
        customWeb.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                if (utils.isSameDomain(postUrl, url)) {
                    val intent = Intent(applicationContext, CustomWebViewActivity::class.java)
                    intent.putExtra("postUrl", url)
                    startActivity(intent)
                } else {
                    openInAppBrowser(url)
                }

                return true
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                progressBar.visibility = View.GONE
            }
        }
        customWeb.clearCache(true)
        customWeb.clearHistory()
        customWeb.settings.javaScriptEnabled = true
        customWeb.isHorizontalScrollBarEnabled = false

        customWeb.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(view: View, event: MotionEvent): Boolean {
                if (event.pointerCount > 1) {
                    return true
                }
                when (event.action) {
                    MotionEvent.ACTION_DOWN -> {
                        mDownX = event.x
                    }

                    MotionEvent.ACTION_MOVE, MotionEvent.ACTION_CANCEL, MotionEvent.ACTION_UP -> {
                        event.setLocation(mDownX, event.y)
                    }
                }
                return false
            }
        })
    }

    private fun renderPosition() {
        customWeb.loadUrl(postUrl)
    }

    private fun openInAppBrowser(url: String) {
        val intent = Intent(applicationContext, BrowserActivity::class.java)
        intent.putExtra("url", url)
        startActivity(intent)
    }

    private fun initCollapsingToolbar() {
        val collapsingToolbar: CollapsingToolbarLayout = findViewById(R.id.webCollapsingToolbar)

        collapsingToolbar.title = " "

        val appBarLayout: AppBarLayout = findViewById(R.id.webAppBar)
        appBarLayout.setExpanded(true)

        appBarLayout.addOnOffsetChangedListener(object : AppBarLayout.OnOffsetChangedListener {
            var isShow: Boolean = false
            var scrollRange: Int = -1

            override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.totalScrollRange
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.title = "Web View"
                    isShow = true
                } else if (isShow) {
                    collapsingToolbar.title = " "
                    isShow = false
                }
            }

        })

        Glide.with(applicationContext)
                .load("https://api.androidhive.info/webview/nougat.jpg")
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageHeader)
    }

    private class MyWebChromeClient(applicationContext: Context) : WebChromeClient()
}
