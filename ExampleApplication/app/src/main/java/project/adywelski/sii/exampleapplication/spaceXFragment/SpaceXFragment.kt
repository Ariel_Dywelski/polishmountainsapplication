package project.adywelski.sii.exampleapplication.spaceXFragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import project.adywelski.sii.exampleapplication.MainApplication
import project.adywelski.sii.exampleapplication.R
import project.adywelski.sii.exampleapplication.spaceXFragment.dto.SpaceXDTO
import project.adywelski.sii.exampleapplication.spaceXFragment.service.SpaceXWebService
import project.adywelski.sii.exampleapplication.spaceXFragment.view.SpaceXRecyclerViewAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject


class SpaceXFragment : Fragment() {

    @Inject lateinit var webService: SpaceXWebService

    private lateinit var recyclerView: RecyclerView

    override fun onAttach(context: Context) {
        super.onAttach(context)

        ((context.applicationContext as MainApplication)).getAppComponent().inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val spaceXFragmentView: View = inflater.inflate(R.layout.fragment_space_x_main, container, false)

        recyclerView = spaceXFragmentView.findViewById(R.id.spaceXRecyclerView)
        recyclerView.layoutManager = LinearLayoutManager(context)

        getSpaceXFlights()

        return spaceXFragmentView
    }

    private fun getSpaceXFlights() {
        webService.getSpaceXFlights("https://api.spacexdata.com/v1/launches?start=2013-01-20&final=2017-05-25")
                .enqueue(object : Callback<List<SpaceXDTO>> {

                    override fun onResponse(call: Call<List<SpaceXDTO>>, response: Response<List<SpaceXDTO>>) {
                        if (response.isSuccessful) {
                            val responseList = response.body()!!

                            Toast.makeText(context, "Space X start success", Toast.LENGTH_LONG).show()

                            val spaceXRecyclerViewAdapter = SpaceXRecyclerViewAdapter(context, responseList)
                            recyclerView.adapter = spaceXRecyclerViewAdapter
                        }
                    }

                    override fun onFailure(call: Call<List<SpaceXDTO>>, t: Throwable?) {
                        Toast.makeText(context, "Failure request", Toast.LENGTH_LONG).show()
                    }
                })
    }
}
