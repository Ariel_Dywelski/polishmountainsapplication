package project.adywelski.sii.exampleapplication.components

import project.adywelski.sii.exampleapplication.MainActivity
import project.adywelski.sii.exampleapplication.MainApplication
import project.adywelski.sii.exampleapplication.customWeb.CustomWebViewActivity
import project.adywelski.sii.exampleapplication.galleryFragmentView.GalleryFragment
import project.adywelski.sii.exampleapplication.newsFragment.NewsFragment
import project.adywelski.sii.exampleapplication.spaceXFragment.SpaceXFragment
import project.adywelski.sii.exampleapplication.swipeLayout.RestaurantMenuActivity
import project.adywelski.sii.exampleapplication.weatherFragment.WeatherFragment

/**
 * Created by ariel_dywelski on 31/10/2017.
 */
interface AppComponent {
    fun inject(application: MainApplication)
    fun inject(activity: MainActivity)
    fun inject(fragment: NewsFragment)
    fun inject(fragment: WeatherFragment)
    fun inject(fragment: GalleryFragment)
    fun inject(fragment: SpaceXFragment)
    fun inject(activity: CustomWebViewActivity)
    fun inject(activity: RestaurantMenuActivity)
}