package project.adywelski.sii.exampleapplication.weatherFragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import project.adywelski.sii.exampleapplication.MainApplication
import project.adywelski.sii.exampleapplication.R
import project.adywelski.sii.exampleapplication.weatherFragment.dto.WeatherDTO
import project.adywelski.sii.exampleapplication.weatherFragment.service.WeatherWebService
import project.adywelski.sii.exampleapplication.weatherFragment.view.WeatherRecyclerViewAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class WeatherFragment : Fragment() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var selectButton: Button
    private lateinit var selectedCitySpinner: Spinner
    private lateinit var cityName: Array<String>

    @Inject lateinit var webService: WeatherWebService

    override fun onAttach(context: Context) {
        super.onAttach(context)

        ((context.applicationContext as MainApplication)).getAppComponent().inject(this)

        cityName = resources.getStringArray(R.array.city_name)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val fragmentView: View = inflater.inflate(R.layout.fragment_weather, container, false)

        recyclerView = fragmentView.findViewById(R.id.weatherRecyclerView)
        selectButton = fragmentView.findViewById(R.id.selectCityButton)
        selectedCitySpinner = fragmentView.findViewById(R.id.citySelectedSpinner)

        recyclerView.layoutManager = LinearLayoutManager(context)

        getTextToSelectCity()

        return fragmentView
    }

    private fun getTextToSelectCity() {
        val spinnerAdapter = ArrayAdapter(context, android.R.layout.simple_dropdown_item_1line, cityName)
        selectedCitySpinner.adapter = spinnerAdapter

        selectedCitySpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val city = cityName[position]
                selectButton.setOnClickListener {
                    getWeather(city)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                selectButton.isEnabled = false
            }
        }
    }

    private fun getWeather(selectCity: String) {
        val url = "http://api.openweathermap.org/data/2.5/find?q=$selectCity&units=metric&appid=1bbf898349092621bb29e3deddddae70"
        webService.getWeatherData(url)
                .enqueue(object : Callback<WeatherDTO> {
                    override fun onResponse(call: Call<WeatherDTO>?, response: Response<WeatherDTO>?) {
                        if (response != null) {
                            if (response.isSuccessful) {
                                val weatherDTO = response.body()
                                if (weatherDTO != null) {
                                    val weatherListDTO = weatherDTO.list

                                    val weatherAdapter = WeatherRecyclerViewAdapter(context, weatherListDTO)
                                    recyclerView.adapter = weatherAdapter
                                }
                                Toast.makeText(context, "Great weather", Toast.LENGTH_LONG).show()
                            } else {
                                Toast.makeText(context, "Unfriendly weather", Toast.LENGTH_LONG).show()
                            }
                        }
                    }

                    override fun onFailure(call: Call<WeatherDTO>?, t: Throwable?) {
                        Toast.makeText(context, "Failure weather", Toast.LENGTH_LONG).show()
                    }
                })
    }
}
