package project.adywelski.sii.exampleapplication.weatherFragment.view

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import project.adywelski.sii.exampleapplication.R
import project.adywelski.sii.exampleapplication.weatherFragment.dto.WeatherListDTO

/**
 * Created by ariel_dywelski on 10/11/2017.
 */
class WeatherRecyclerViewAdapter(val context: Context, private val weatherListDTO: List<WeatherListDTO>) : RecyclerView.Adapter<WeatherViewHolder>() {

    private val layoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): WeatherViewHolder {
        val view: View = layoutInflater.inflate(R.layout.fragment_weather_view_item, parent, false)
        return WeatherViewHolder(view)
    }

    override fun onBindViewHolder(holder: WeatherViewHolder, position: Int) {
        val weatherDTO: WeatherListDTO = weatherListDTO[position]
        holder.bind(weatherListDTO, position)
    }

    override fun getItemCount(): Int {
        return weatherListDTO.size
    }
}