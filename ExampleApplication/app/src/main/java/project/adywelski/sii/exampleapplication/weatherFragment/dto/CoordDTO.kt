package project.adywelski.sii.exampleapplication.weatherFragment.dto

/**
 * Created by ariel_dywelski on 09/11/2017.
 */
data class CoordDTO(val lat: Double,
                    val lon: Double)