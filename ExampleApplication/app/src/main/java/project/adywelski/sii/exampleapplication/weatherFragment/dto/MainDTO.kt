package project.adywelski.sii.exampleapplication.weatherFragment.dto

/**
 * Created by ariel_dywelski on 09/11/2017.
 */
data class MainDTO (val temp: Double,
                    val pressure: Double,
                    val humidity: Int,
                    val temp_min: Double,
                    val temp_max: Double)