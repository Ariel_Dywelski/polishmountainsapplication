package project.adywelski.sii.exampleapplication.spaceXFragment.dto

/**
 * Created by ariel_dywelski on 14/11/2017.
 */
data class SpaceXLinksDTO(val mission_patch: String,
                          val article_link: String,
                          val video_link: String)