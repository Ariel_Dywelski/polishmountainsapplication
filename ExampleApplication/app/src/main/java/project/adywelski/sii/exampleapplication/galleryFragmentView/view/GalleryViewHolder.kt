package project.adywelski.sii.exampleapplication.galleryFragmentView.view

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import project.adywelski.sii.exampleapplication.R
import project.adywelski.sii.exampleapplication.settings.SettingActivity
import project.adywelski.sii.exampleapplication.galleryFragmentView.dto.PictureDTO

/**
 * Created by ariel_dywelski on 03/11/2017.
 */
class GalleryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
    val picture : ImageView = itemView.findViewById(R.id.pictureRecyclerViewImageView)

    fun pictureClick(context: Context, pictureDTO: PictureDTO) {
        itemView.setOnClickListener{
            val position : Int = adapterPosition
            if (position != RecyclerView.NO_POSITION) {
                Toast.makeText(context, "Clicked picture number $position", Toast.LENGTH_SHORT).show()

                val regular = pictureDTO.regular
                val pictureIntent = Intent(context, SettingActivity::class.java)

                pictureIntent.putExtra("ImageViewUrl", regular)

                context.startActivity(pictureIntent)
            }
        }
    }
}