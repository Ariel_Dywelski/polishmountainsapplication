package project.adywelski.sii.exampleapplication.weatherFragment.view

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import project.adywelski.sii.exampleapplication.R
import project.adywelski.sii.exampleapplication.weatherFragment.dto.WeatherListDTO

/**
 * Created by ariel_dywelski on 10/11/2017.
 */
class WeatherViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val weatherIcon : ImageView = itemView.findViewById(R.id.bigWeatherIcon)
    private val cityName: TextView = itemView.findViewById(R.id.cityName)
    private val minTemp: TextView = itemView.findViewById(R.id.minTemperature)
    private val maxTemp: TextView = itemView.findViewById(R.id.maxTemperature)
    private val humidity: TextView = itemView.findViewById(R.id.humidity)
    private val pressure: TextView = itemView.findViewById(R.id.pressure)

    fun bind(weatherListDTO: List<WeatherListDTO>, position: Int){
        weatherIcon.setImageResource(R.drawable.icons8_clouds)
        val weatherListPosition = weatherListDTO[position]
        val generalWeather = weatherListPosition.main

        cityName.text = weatherListPosition.name
        minTemp.text = generalWeather.temp_min.toString()
        maxTemp.text = generalWeather.temp_max.toString()
        humidity.text = generalWeather.humidity.toString()
        pressure.text = generalWeather.pressure.toString()
    }
}