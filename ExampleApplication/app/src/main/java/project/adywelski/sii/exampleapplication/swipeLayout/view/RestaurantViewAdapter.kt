package project.adywelski.sii.exampleapplication.swipeLayout.view

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import project.adywelski.sii.exampleapplication.R
import project.adywelski.sii.exampleapplication.swipeLayout.dto.Item

/**
 * Created by ariel_dywelski on 29/11/2017.
 */
class RestaurantViewAdapter(val context: Context, private val cartList: ArrayList<Item>): RecyclerView.Adapter<RestaurantViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RestaurantViewHolder {
        val itemView: View = LayoutInflater.from(parent.context).inflate(R.layout.activity_restaurant_menu_item, parent, false)

        return RestaurantViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: RestaurantViewHolder, position: Int) {
        val item: Item = cartList[position]

        holder.name.text = item.name
        holder.description.text = item.description
        holder.price.text = item.price.toString()

        Glide.with(context)
                .load(item.thumbnail)
                .into(holder.thumbnail)
    }

    override fun getItemCount(): Int {
        return cartList.size
    }

    fun removeItem(position: Int){
        cartList.removeAt(position)
        notifyItemRemoved(position)
    }

    fun restoreItem(item: Item, position: Int){
        cartList.add(position, item)
        notifyItemInserted(position)
    }
}