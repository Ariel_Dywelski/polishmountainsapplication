package project.adywelski.sii.exampleapplication.spaceXFragment.service

import project.adywelski.sii.exampleapplication.spaceXFragment.dto.SpaceXDTO
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Url

/**
 * Created by ariel_dywelski on 14/11/2017.
 */
interface SpaceXWebService {

    @GET
    fun getSpaceXFlights(@Url url: String) : Call<List<SpaceXDTO>>
}