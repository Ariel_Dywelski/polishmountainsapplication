package project.adywelski.sii.exampleapplication.customWeb

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.view.Menu
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.webkit.*
import android.widget.ProgressBar
import kotlinx.android.synthetic.main.activity_custom_web_view.*
import project.adywelski.sii.exampleapplication.R



class BrowserActivity : AppCompatActivity() {

    private lateinit var url: String
    private lateinit var webView: WebView
    private lateinit var progressBar: ProgressBar
    private var mDownX: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_browser)
        setSupportActionBar(webToolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""

        url = intent.getStringExtra("url")

        if (TextUtils.isEmpty(url)) {
            finish()
        }

        webView = findViewById(R.id.customWebView)
        progressBar = findViewById(R.id.webProgressBar)

        initWebView()

        webView.loadUrl(url)
    }

    @SuppressLint("SetJavaScriptEnabled", "ClickableViewAccessibility")
    private fun initWebView(){
        webView.webChromeClient = MyWebChromeClient(applicationContext)
        webView.webViewClient = object: WebViewClient() {

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                progressBar.visibility = View.VISIBLE
                invalidateOptionsMenu()
            }

            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                webView.loadUrl(url)
                return true
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                progressBar.visibility = View.GONE
                invalidateOptionsMenu()
            }

            override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError?) {
                super.onReceivedError(view, request, error)
                progressBar.visibility = View.GONE
                invalidateOptionsMenu()
            }
        }
        webView.clearCache(true)
        webView.clearHistory()
        webView.settings.javaScriptEnabled = true
        webView.isHorizontalScrollBarEnabled = false

        webView.setOnTouchListener(object: View.OnTouchListener {
            override fun onTouch(view: View, event: MotionEvent): Boolean {
                if (event.pointerCount > 1) {
                    return true
                }
                when (event.action) {
                    MotionEvent.ACTION_DOWN -> {
                        mDownX = event.x.toInt()
                    }

                    MotionEvent.ACTION_MOVE, MotionEvent.ACTION_CANCEL, MotionEvent.ACTION_UP -> {
                        event.setLocation(mDownX.toFloat(), event.y)
                    }
                }
                return false
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.browser, menu)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {

        if (!webView.canGoBack()) {
            menu.getItem(0).isEnabled = false
            menu.getItem(0).icon.alpha = 130
        } else {
            menu.getItem(0).isEnabled = true
            menu.getItem(0).icon.alpha = 255
        }

        if (!webView.canGoForward()) {
            menu.getItem(1).isEnabled = false
            menu.getItem(1).icon.alpha = 130
        } else {
            menu.getItem(1).isEnabled = true
            menu.getItem(1).icon.alpha = 255
        }

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if (item.itemId == android.R.id.home) {
            finish()
        }
        if (item.itemId == R.id.action_back) {
            back()
        }
        if (item.itemId == R.id.action_forward) {
            forward()
        }

        return super.onOptionsItemSelected(item)
    }

    private fun back() {
        if (webView.canGoBack()) {
            webView.goBack()
        }
    }

    private fun forward() {
        if (webView.canGoForward()) {
            webView.goForward()
        }
    }

    private class MyWebChromeClient(private val context: Context): WebChromeClient()
}
