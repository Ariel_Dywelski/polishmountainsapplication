package project.adywelski.sii.exampleapplication

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Window
import android.view.WindowManager



class SplashScreen : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_splash_screen)

        val welcomeThread = object : Thread() {

            override fun run() {
                try {
                    super.run()
                    Thread.sleep(1000)
                } catch (e: Exception) {

                } finally {

                    val i = Intent(applicationContext,
                            MainActivity::class.java)
                    startActivity(i)
                    finish()
                }
            }
        }
        welcomeThread.start()
    }
}
