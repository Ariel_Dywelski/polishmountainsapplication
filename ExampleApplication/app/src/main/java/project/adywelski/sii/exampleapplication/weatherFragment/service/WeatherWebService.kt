package project.adywelski.sii.exampleapplication.weatherFragment.service

import project.adywelski.sii.exampleapplication.weatherFragment.dto.WeatherDTO
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Url

/**
 * Created by ariel_dywelski on 10/11/2017.
 */
interface WeatherWebService {

    @GET
    fun getWeatherData(@Url url: String) : Call<WeatherDTO>

    /**
     * URL to data - http://api.openweathermap.org/data/2.5/find?q=Gdansk&units=metric&appid=1bbf898349092621bb29e3deddddae70
     */
}