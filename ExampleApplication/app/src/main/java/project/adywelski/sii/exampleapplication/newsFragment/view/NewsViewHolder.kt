package project.adywelski.sii.exampleapplication.newsFragment.view

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import project.adywelski.sii.exampleapplication.R
import project.adywelski.sii.exampleapplication.newsFragment.ArticlesDTO

/**
 * Created by ariel_dywelski on 27/10/2017.
 */
class NewsViewHolder(itemView: View, val context: Context) : RecyclerView.ViewHolder(itemView) {

    private val articleTitle: TextView = itemView.findViewById(R.id.articleTitleText)
    private val articleDescription: TextView = itemView.findViewById(R.id.articleDescription)
    private val articleURL: TextView = itemView.findViewById(R.id.articleURLText)
    private val articlePhoto: ImageView = itemView.findViewById(R.id.articlePhoto)

    fun bind(articlesDTO: ArticlesDTO) {
        articleTitle.text = articlesDTO.title
        articleDescription.text = articlesDTO.description
        articleURL.text = articlesDTO.url

        articleURL.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(articlesDTO.url))
            context.startActivity(browserIntent)
        }

        Glide.with(context)
                .load(articlesDTO.urlToImage)
                .into(articlePhoto)
    }
}
