package project.adywelski.sii.exampleapplication.spaceXFragment.view

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import project.adywelski.sii.exampleapplication.R
import project.adywelski.sii.exampleapplication.spaceXFragment.dto.SpaceXDTO

/**
 * Created by ariel_dywelski on 14/11/2017.
 */
class SpaceXRecyclerViewAdapter(val context: Context, private val spaceXDTOList: List<SpaceXDTO>): RecyclerView.Adapter<SpaceXViewHolder>() {

    private val layoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): SpaceXViewHolder {
        val view: View = layoutInflater.inflate(R.layout.fragment_space_x_main_view_item, parent, false)
        return SpaceXViewHolder(view, context)
    }

    override fun onBindViewHolder(holder: SpaceXViewHolder, position: Int) {
        val spaceXDTO: SpaceXDTO = spaceXDTOList[position]
        holder.bind(spaceXDTOList, position)
    }

    override fun getItemCount(): Int {
        return spaceXDTOList.size
    }
}
