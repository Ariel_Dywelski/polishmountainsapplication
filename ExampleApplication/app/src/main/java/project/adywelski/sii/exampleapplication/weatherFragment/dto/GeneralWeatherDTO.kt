package project.adywelski.sii.exampleapplication.weatherFragment.dto

/**
 * Created by ariel_dywelski on 09/11/2017.
 */
data class GeneralWeatherDTO(val id: Int,
                             val main: String,
                             val description: String,
                             val icon: String)