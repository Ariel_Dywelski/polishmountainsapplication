package project.adywelski.sii.exampleapplication.galleryFragmentView.dto

import project.adywelski.sii.exampleapplication.galleryFragmentView.dto.PictureDTO

/**
 * Created by ariel_dywelski on 31/10/2017.
 */
data class UnsplashDTO(val id: String,
                       val urls: PictureDTO)