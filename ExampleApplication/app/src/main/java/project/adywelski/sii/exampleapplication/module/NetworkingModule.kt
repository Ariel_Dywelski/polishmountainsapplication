package project.adywelski.sii.exampleapplication.module

import android.content.Context
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import project.adywelski.sii.exampleapplication.galleryFragmentView.service.UnsplashWebService
import project.adywelski.sii.exampleapplication.newsFragment.NewsApiWebService
import project.adywelski.sii.exampleapplication.spaceXFragment.service.SpaceXWebService
import project.adywelski.sii.exampleapplication.weatherFragment.service.WeatherWebService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * Created by ariel_dywelski on 31/10/2017.
 */
@Module
class NetworkingModule(private val applicationContext: Context) {

    val BASE_URL: String = "https://pixabay.com/api/?key=6388153-ca755fe28d71b7583a1021e82"

    @Provides
    @Singleton
    fun provideApplicationContext() = applicationContext

    @Provides
    @Singleton
    fun provideGson(): Gson {
        return Gson()
    }

    @Provides
    @Singleton
    fun provideOKHttpClient(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        val builder = OkHttpClient.Builder()
        builder.addInterceptor(interceptor)

        return builder.build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build()
    }

    @Provides
    @Singleton
    fun providePixabayWebService(retrofit: Retrofit): UnsplashWebService {
        return retrofit.create(UnsplashWebService::class.java)
    }

    @Provides
    @Singleton
    fun provideNewsWebService(retrofit: Retrofit): NewsApiWebService {
        return retrofit.create(NewsApiWebService::class.java)
    }

    @Provides
    @Singleton
    fun provideWeatherWebService(retrofit: Retrofit): WeatherWebService{
        return retrofit.create(WeatherWebService::class.java)
    }

    @Provides
    @Singleton
    fun provideSpaceXWebService(retrofit: Retrofit): SpaceXWebService{
        return retrofit.create(SpaceXWebService::class.java)
    }
}