package project.adywelski.sii.exampleapplication.newsFragment

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Url

/**
 * Created by ariel_dywelski on 06/11/2017.
 */
interface NewsApiWebService {

    @GET
    fun getArticles(@Url url: String): Call<NewsDTO>

    @GET
    fun getSources(@Url url: String): Call<GeneralSourcesDTO>
}