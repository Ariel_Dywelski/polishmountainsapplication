package project.adywelski.sii.exampleapplication.newsFragment

/**
 * Created by ariel_dywelski on 30/10/2017.
 */
data class NewsDTO(val source : String,
                   val sortBy : String,
                   val articles : List<ArticlesDTO>)
