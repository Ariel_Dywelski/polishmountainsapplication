package project.adywelski.sii.exampleapplication.galleryFragmentView.service

import project.adywelski.sii.exampleapplication.galleryFragmentView.dto.UnsplashDTO
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Url

/**
 * Created by ariel_dywelski on 31/10/2017.
 */
interface UnsplashWebService {

    @GET
    fun getUnsplashPhoto(@Url url: String) : Call<List<UnsplashDTO>>
}