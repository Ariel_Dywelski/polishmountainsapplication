package project.adywelski.sii.exampleapplication.swipeLayout

import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.Menu
import android.view.View
import android.widget.Toast
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_restaurant_menu.*
import project.adywelski.sii.exampleapplication.MainApplication
import project.adywelski.sii.exampleapplication.R
import project.adywelski.sii.exampleapplication.swipeLayout.dto.Item
import project.adywelski.sii.exampleapplication.swipeLayout.view.RestaurantViewAdapter
import project.adywelski.sii.exampleapplication.swipeLayout.view.RestaurantViewHolder


class RestaurantMenuActivity : AppCompatActivity(), RecyclerItemTouchHelperListener {

    private val requestUrl: String = "https://api.androidhive.info/json/menu.json"

    private lateinit var recyclerView: RecyclerView
    private lateinit var cardList: ArrayList<Item>
    private lateinit var adapter: RestaurantViewAdapter
    private lateinit var coordinatorLayout: CoordinatorLayout

    private val activityTag: String = "MainActivityTag"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_restaurant_menu)
        setSupportActionBar(toolbarRestaurant)

        ((applicationContext as MainApplication)).getAppComponent().inject(this)

        supportActionBar?.title = getString(R.string.my_cart)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        recyclerView = findViewById(R.id.restaurantRecyclerView)
        coordinatorLayout = findViewById(R.id.coordinator_layout)
        cardList = arrayListOf()
        adapter = RestaurantViewAdapter(applicationContext, cardList)

        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(applicationContext)

        recyclerView.layoutManager = layoutManager
        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerView.addItemDecoration(DividerItemDecoration(applicationContext, DividerItemDecoration.VERTICAL))
        recyclerView.adapter = adapter

        val itemTouchHelperCallback: ItemTouchHelper.SimpleCallback = RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this)

        ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView)

        prepareCard()

    }

    private fun prepareCard() {
        val request = JsonArrayRequest(requestUrl,
                Response.Listener { response ->
                    if (response == null) {
                        Toast.makeText(applicationContext, "Couldn't fetch the menu! Pleas try again.", Toast.LENGTH_LONG).show()
                    }

                    val items = Gson().fromJson<List<Item>>(response.toString(), object : TypeToken<List<Item>>() {

                    }.type)

                    // adding items to cart list
                    cardList.clear()
                    cardList.addAll(items)

                    // refreshing recycler view
                    adapter.notifyDataSetChanged()
                }, Response.ErrorListener { error ->
            Toast.makeText(applicationContext, "Error: " + error.message, Toast.LENGTH_SHORT).show()
        })
        ((applicationContext as MainApplication)).addToRequestQueue(request)
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int, position: Int) {
        if (viewHolder is RestaurantViewHolder) {
            val name = cardList[viewHolder.adapterPosition].name

            val deletedItem: Item = cardList[viewHolder.adapterPosition]
            val deletedIndex: Int = viewHolder.adapterPosition

            adapter.removeItem(deletedIndex)

            val snackbar: Snackbar = Snackbar.make(coordinatorLayout, name + " removed from card!", Snackbar.LENGTH_LONG)
            snackbar.setAction("UNDO", object: View.OnClickListener{
                override fun onClick(v: View?) {
                    adapter.restoreItem(deletedItem, deletedIndex)
                }
            })
            snackbar.setActionTextColor(Color.YELLOW)
            snackbar.show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }
}
