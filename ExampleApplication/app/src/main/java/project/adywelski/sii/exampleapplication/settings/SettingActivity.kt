package project.adywelski.sii.exampleapplication.settings

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Window
import android.view.WindowManager
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_setting.*
import project.adywelski.sii.exampleapplication.R

class SettingActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_setting)

        val image = intent.getStringExtra("ImageViewUrl")

        Glide.with(applicationContext)
                .load(image)
                .fitCenter()
                .into(intentImage)
    }
}
