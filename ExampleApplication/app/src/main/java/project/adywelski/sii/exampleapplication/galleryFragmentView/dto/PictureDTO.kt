package project.adywelski.sii.exampleapplication.galleryFragmentView.dto

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by ariel_dywelski on 03/11/2017.
 */
data class PictureDTO(val raw: String,
                      val full: String,
                      val regular: String,
                      val small: String,
                      val thumb: String) : Parcelable{

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(raw)
        parcel.writeString(full)
        parcel.writeString(regular)
        parcel.writeString(small)
        parcel.writeString(thumb)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PictureDTO> {
        override fun createFromParcel(parcel: Parcel): PictureDTO {
            return PictureDTO(parcel)
        }

        override fun newArray(size: Int): Array<PictureDTO?> {
            return arrayOfNulls(size)
        }
    }

}