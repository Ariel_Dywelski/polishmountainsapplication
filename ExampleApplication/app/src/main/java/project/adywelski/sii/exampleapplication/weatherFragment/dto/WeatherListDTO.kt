package project.adywelski.sii.exampleapplication.weatherFragment.dto

/**
 * Created by ariel_dywelski on 09/11/2017.
 */
data class WeatherListDTO (val id: Int,
                           val name: String,
                           val coord: CoordDTO,
                           val main: MainDTO,
                           val dt: Int,
                           val wind: WindDTO,
                           val sys: SysDTO,
                           val rain: Any,
                           val snow: Any,
                           val clouds: CloudsDTO,
                           val weather: List<GeneralWeatherDTO>)