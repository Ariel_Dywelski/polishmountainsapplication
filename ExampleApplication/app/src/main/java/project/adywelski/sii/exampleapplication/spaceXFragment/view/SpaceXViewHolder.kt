package project.adywelski.sii.exampleapplication.spaceXFragment.view

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import project.adywelski.sii.exampleapplication.R
import project.adywelski.sii.exampleapplication.spaceXFragment.dto.SpaceXDTO

/**
 * Created by ariel_dywelski on 14/11/2017.
 */
class SpaceXViewHolder(itemView: View, val context: Context) : RecyclerView.ViewHolder(itemView) {

    private val articleLinkTextView: TextView = itemView.findViewById(R.id.spaceXArticleLink)
    private val missionPatchImage: ImageView = itemView.findViewById(R.id.spaceXMissionPatch)
    private val videoLinkTextView: TextView = itemView.findViewById(R.id.spaceXVideoLink)
    private val descriptionTextView: TextView = itemView.findViewById(R.id.spaceXDescription)

    fun bind(spaceXDTO: List<SpaceXDTO>, position: Int){

        val spaceXLinksDTO = spaceXDTO[position].links
        val articleLink = spaceXLinksDTO.article_link
        val imageLink = spaceXLinksDTO.mission_patch
        val videoLink = spaceXLinksDTO.video_link
        val description = spaceXDTO[position].details

        articleLinkTextView.text = articleLink
        videoLinkTextView.text = videoLink
        descriptionTextView.text = description

        Glide.with(context)
                .load(imageLink)
                .into(missionPatchImage)

        articleLinkTextView.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(articleLink))
            context.startActivity(browserIntent)
        }

        videoLinkTextView.setOnClickListener {
            val videoIntent = Intent(Intent.ACTION_VIEW, Uri.parse(videoLink))
            context.startActivity(videoIntent)
        }
    }
}
