package project.adywelski.sii.exampleapplication

import android.app.Application
import android.text.TextUtils
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley
import project.adywelski.sii.exampleapplication.components.AppComponent
import project.adywelski.sii.exampleapplication.components.DaggerAppStubComponent
import project.adywelski.sii.exampleapplication.module.AndroidModule
import project.adywelski.sii.exampleapplication.module.NetworkingModule


/**
 * Created by ariel_dywelski on 31/10/2017.
 */
class MainApplication : Application() {
    private lateinit var component: AppComponent
    private lateinit var requestQueue: RequestQueue
    private val applicationTag: String = "MainApplication"

    fun getAppComponent(): AppComponent {
        return component
    }

    override fun onCreate() {
        super.onCreate()

        component = DaggerAppStubComponent
                .builder()
                .androidModule(AndroidModule(this))
                .networkingModule(NetworkingModule(this))
                .build()

        component.inject(this)
    }

    fun getRequestQueue(): RequestQueue {
        requestQueue = Volley.newRequestQueue(applicationContext)

        return requestQueue
    }

    fun <T> addToRequestQueue(req: Request<T>, tag: String) {
        req.tag = if (TextUtils.isEmpty(tag)) applicationTag else tag
        getRequestQueue().add(req)
    }

    fun <T> addToRequestQueue(req: Request<T>) {
        req.tag = applicationTag
        getRequestQueue().add(req)
    }

    fun cancelPendingRequests(tag: Any) {
        requestQueue.cancelAll(tag)
    }

}