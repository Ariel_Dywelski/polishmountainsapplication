package project.adywelski.sii.exampleapplication

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

/**
 * Created by ariel_dywelski on 27/10/2017.
 */
class ViewPagerAdapter (manager: FragmentManager) : FragmentPagerAdapter(manager) {

    private val fragmentList : ArrayList<Fragment> = arrayListOf()
    private val fragmentTitleList : ArrayList<String> = arrayListOf()

    override fun getItem(position: Int): Fragment {
        return fragmentList[position]
    }

    override fun getCount(): Int {
        return fragmentList.size
    }

    fun addFragment(fragment: Fragment, title : String){
        fragmentList.add(fragment)
        fragmentTitleList.add(title)
    }

    override fun getPageTitle(position: Int): CharSequence {
        return fragmentTitleList[position]
    }

    fun clearFragmentList(){
        fragmentList.clear()
        fragmentTitleList.clear()
    }
}