package project.adywelski.sii.exampleapplication.swipeLayout.dto

/**
 * Created by ariel_dywelski on 29/11/2017.
 */
data class Item(val id: Int,
                val name: String,
                val description: String,
                val price: Double,
                val thumbnail: String)