package project.adywelski.sii.exampleapplication.swipeLayout.view

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import project.adywelski.sii.exampleapplication.R

/**
 * Created by ariel_dywelski on 29/11/2017.
 */
class RestaurantViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val name : TextView = itemView.findViewById(R.id.name)
    val description: TextView = itemView.findViewById(R.id.description)
    val price: TextView = itemView.findViewById(R.id.price)
    val thumbnail: ImageView = itemView.findViewById(R.id.thumbnail)
    val viewBackground: RelativeLayout = itemView.findViewById(R.id.view_background)
    val viewForeground: RelativeLayout = itemView.findViewById(R.id.view_foreground)
}