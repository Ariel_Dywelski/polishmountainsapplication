package project.adywelski.sii.exampleapplication

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.support.design.widget.NavigationView
import android.support.design.widget.Snackbar
import android.support.design.widget.TabLayout
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.support.v4.view.ViewPager
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_menu.*
import project.adywelski.sii.exampleapplication.customWeb.CustomWebViewActivity
import project.adywelski.sii.exampleapplication.galleryFragmentView.GalleryFragment
import project.adywelski.sii.exampleapplication.newsFragment.NewsFragment
import project.adywelski.sii.exampleapplication.spaceXFragment.SpaceXFragment
import project.adywelski.sii.exampleapplication.swipeLayout.RestaurantMenuActivity
import project.adywelski.sii.exampleapplication.weatherFragment.WeatherFragment


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private val requestImageCapture = 1
    private val viewPagerAdapter = ViewPagerAdapter(supportFragmentManager)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbarMain)
        createFloatingButton()

        ((application as MainApplication)).getAppComponent().inject(this)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        setupViewPager(viewPager)

        tabLayout.setupWithViewPager(viewPager)
        tabLayout.setOnTabSelectedListener(onTabSelectedListener(viewPager))

        checkPermission()

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbarMain, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
    }

    private fun onTabSelectedListener(viewPager: ViewPager): TabLayout.OnTabSelectedListener{
        return object: TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager.currentItem = tab.position
            }

        }
    }

    private fun setupViewPager(viewPager: ViewPager) {
        viewPagerAdapter.clearFragmentList()

        viewPagerAdapter.addFragment(NewsFragment(), "News")
        viewPagerAdapter.addFragment(GalleryFragment(), "Gallery")
        viewPagerAdapter.addFragment(WeatherFragment(), "Weather")
        viewPagerAdapter.addFragment(SpaceXFragment(), "SpaceX")
        viewPager.adapter = viewPagerAdapter
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        if (requestCode == requestImageCapture && resultCode == Activity.RESULT_OK) {

        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == 123) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

            }
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_camera -> {
                val pictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(pictureIntent, requestImageCapture)
            }
            R.id.nav_internetBrowser -> {
                val browserIntent = Intent(applicationContext, CustomWebViewActivity::class.java)
                startActivity(browserIntent)
            }
            R.id.nav_slideshow -> {
                Toast.makeText(this, "SLIDESHOW", Toast.LENGTH_SHORT).show()
                val restaurantIntent = Intent(applicationContext, RestaurantMenuActivity::class.java)
                startActivity(restaurantIntent)
            }
            R.id.nav_manage -> {
                Toast.makeText(this, "MANAGE", Toast.LENGTH_SHORT).show()
            }
            R.id.nav_settings -> {
                Toast.makeText(this, "SETTINGS", Toast.LENGTH_SHORT).show()
                val intent = Intent(Settings.ACTION_SETTINGS)
                startActivity(intent)
            }
            R.id.nav_share -> {
                Toast.makeText(this, "SHARE", Toast.LENGTH_SHORT).show()
            }
            R.id.nav_send -> {
                Toast.makeText(this, "SEND", Toast.LENGTH_SHORT).show()
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun checkPermission(): Boolean {
        if (ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE), 123)
            return true
        }
        return false
    }

    private fun createFloatingButton() {
        val snackBarText = "Welcome. Send email to developer."
        floatingActionButtonMainActivity.setOnClickListener { view ->

            val snackBar = Snackbar.make(view, snackBarText, Snackbar.LENGTH_LONG)
            val snackBarView = snackBar.view
            val snackBarTextView: TextView = snackBarView.findViewById(android.support.design.R.id.snackbar_text)
            snackBarTextView.textAlignment = View.TEXT_ALIGNMENT_CENTER
            snackBar.show()

            sendingEmailToDeveloper()
        }
    }

    private fun sendingEmailToDeveloper() {
        val emailIntent = Intent(Intent.ACTION_SEND)
        val emails = arrayOf("arieldywelski@poczta.onet.pl", "adywelski@pl.sii.eu")
        emailIntent.type = "text/plain"
        emailIntent.putExtra(Intent.EXTRA_EMAIL, emails)
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Add your subject: ")
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Dear developer ... ")

        startActivity(Intent.createChooser(emailIntent, "Send feedback:"))
    }
}

