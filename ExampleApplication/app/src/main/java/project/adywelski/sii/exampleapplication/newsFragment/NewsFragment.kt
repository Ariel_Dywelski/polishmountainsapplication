package project.adywelski.sii.exampleapplication.newsFragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import project.adywelski.sii.exampleapplication.MainApplication
import project.adywelski.sii.exampleapplication.R
import project.adywelski.sii.exampleapplication.newsFragment.view.NewsRecyclerViewAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject


class NewsFragment : Fragment() {

    private lateinit var recyclerView: RecyclerView
    @Inject lateinit var webService: NewsApiWebService

    private var source: String? = null
    private val apiKey: String = "&apiKey=d868276b98934da6b235781b3fd7c221"

    private val url = "https://newsapi.org/v1/articles?source=$source&apiKey=d868276b98934da6b235781b3fd7c221"
    private val sourceUrl: String = "https://newsapi.org/v1/sources"
    private lateinit var sourceSpinner: Spinner

    private var articlesList: List<ArticlesDTO> = arrayListOf()
    private var sourcesList: MutableList<String> = arrayListOf()

    override fun onAttach(context: Context) {
        super.onAttach(context)

        ((context.applicationContext as MainApplication)).getAppComponent().inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView: View = inflater!!.inflate(R.layout.fragment_news, container, false)

        recyclerView = rootView.findViewById(R.id.mainFragmentRecyclerView)
        sourceSpinner = rootView.findViewById(R.id.sourceSpinner)
        recyclerView.layoutManager = LinearLayoutManager(context)

        getSources()

        return rootView
    }

    private fun getSources(): Boolean {
        webService.getSources(sourceUrl).enqueue(object : Callback<GeneralSourcesDTO> {
            override fun onResponse(call: Call<GeneralSourcesDTO>?, response: Response<GeneralSourcesDTO>?) {
                if (response != null) {
                    if (response.isSuccessful) {
                        Toast.makeText(context, "Success source downloading", Toast.LENGTH_SHORT).show()
                        val sourceResponseBody = response.body()!!

                        val sources = sourceResponseBody.sources

                        for (i in 0 until sources.size-1) {
                            sourcesList.add(sources[i].id)
                        }

                        val spinnerArrayAdapter = ArrayAdapter(context, android.R.layout.simple_spinner_dropdown_item, sourcesList)
                        sourceSpinner.adapter = spinnerArrayAdapter

                        var selectedSource: String
                        sourceSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                                selectedSource = sources[position].id

                                source= selectedSource
                                getNews(source!!)
                            }

                            override fun onNothingSelected(parent: AdapterView<*>?) {
                                val selectedSource = "bbc-news"
                                getNews(selectedSource)
                            }
                        }
                    } else {
                        Toast.makeText(context, "Something wrong with downloading", Toast.LENGTH_SHORT).show()
                    }
                }
            }

            override fun onFailure(call: Call<GeneralSourcesDTO>?, t: Throwable?) {
                Toast.makeText(context, "Failure", Toast.LENGTH_SHORT).show()
            }
        })
        return true
    }

    private fun getNews(source: String) {
        webService.getArticles("https://newsapi.org/v1/articles?source=$source&apiKey=d868276b98934da6b235781b3fd7c221").enqueue(object : Callback<NewsDTO> {
            override fun onResponse(call: Call<NewsDTO>?, response: Response<NewsDTO>?) {
                if (response != null) {
                    if (response.isSuccessful) {
                        Toast.makeText(context, "Success downloading", Toast.LENGTH_SHORT).show()
                        val newsResponseBody = response.body()!!
                        articlesList = newsResponseBody.articles

                        val mainFragmentRecyclerViewAdapter = NewsRecyclerViewAdapter(context, articlesList)
                        recyclerView.adapter = mainFragmentRecyclerViewAdapter

                    } else {
                        Toast.makeText(context, "Something wrong", Toast.LENGTH_SHORT).show()
                    }
                }
            }

            override fun onFailure(call: Call<NewsDTO>?, t: Throwable?) {
                Toast.makeText(context, "Failure", Toast.LENGTH_SHORT).show()
            }
        })
    }
}