package project.adywelski.sii.exampleapplication.spaceXFragment.dto

/**
 * Created by ariel_dywelski on 14/11/2017.
 */
data class RocketDTO(val rocket: String,
                     val rocket_name: String,
                     val rocket_type: String)