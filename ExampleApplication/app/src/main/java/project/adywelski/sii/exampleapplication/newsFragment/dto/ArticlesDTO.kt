package project.adywelski.sii.exampleapplication.newsFragment

/**
 * Created by ariel_dywelski on 06/11/2017.
 */
data class ArticlesDTO(val author : String,
                       val title : String,
                       val description : String,
                       val url: String,
                       val urlToImage : String)