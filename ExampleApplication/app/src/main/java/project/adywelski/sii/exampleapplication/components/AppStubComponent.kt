package project.adywelski.sii.exampleapplication.components

import dagger.Component
import project.adywelski.sii.exampleapplication.module.AndroidModule
import project.adywelski.sii.exampleapplication.module.NetworkingModule
import javax.inject.Singleton

/**
 * Created by ariel_dywelski on 31/10/2017.
 */
@Singleton
@Component(modules = arrayOf(AndroidModule::class, NetworkingModule::class))
interface AppStubComponent : AppComponent{
}