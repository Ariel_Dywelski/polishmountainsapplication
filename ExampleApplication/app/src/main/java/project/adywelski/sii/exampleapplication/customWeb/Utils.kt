package project.adywelski.sii.exampleapplication.customWeb

import android.content.Context
import android.graphics.PorterDuff
import android.support.v4.content.ContextCompat
import android.view.MenuItem


/**
 * Created by ariel_dywelski on 28/11/2017.
 */
open class Utils {

    fun isSameDomain(url: String, url1: String): Boolean{
        return getRootDomainUrl(url.toLowerCase()) == getRootDomainUrl(url1.toLowerCase())
    }

    private fun getRootDomainUrl(url: String): String {
        val domainKeys = url.split("/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[2].split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val length = domainKeys.size
        val dummy = if (domainKeys[0] == "www") 1 else 0
        return if (length - dummy == 2)
            domainKeys[length - 2] + "." + domainKeys[length - 1]
        else {
            if (domainKeys[length - 1].length == 2) {
                domainKeys[length - 3] + "." + domainKeys[length - 2] + "." + domainKeys[length - 1]
            } else {
                domainKeys[length - 2] + "." + domainKeys[length - 1]
            }
        }
    }

    fun tintMenuIcon(context: Context, item: MenuItem, color: Int) {
        val drawable = item.icon
        if (drawable != null) {
            drawable.mutate()
            drawable.setColorFilter(ContextCompat.getColor(context, color), PorterDuff.Mode.SRC_ATOP)
        }
    }

    fun bookmarkUrl(context: Context, url: String) {
        val pref = context.getSharedPreferences("arielUrl", 0)
        val editor = pref.edit()

        if (pref.getBoolean(url, false)) {
            editor.putBoolean(url, false)
        } else {
            editor.putBoolean(url, true)
        }

        editor.apply()
    }

    fun isBookmarked(context: Context, url: String): Boolean {
        val pref = context.getSharedPreferences("arielUrl", 0)
        return pref.getBoolean(url, false)
    }

}